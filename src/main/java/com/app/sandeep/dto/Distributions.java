package com.app.sandeep.dto;





public class Distributions {
	
private String assignmentCategory;
private Integer weight;
public Distributions() {
	super();
}
public Distributions(String assignmentCategory, Integer weight) {
	super();
	this.assignmentCategory = assignmentCategory;
	this.weight = weight;
}
public String getAssignmentCategory() {
	return assignmentCategory;
}
public void setAssignmentCategory(String assignmentCategory) {
	this.assignmentCategory = assignmentCategory;
}
public Integer getWeight() {
	return weight;
}
public void setWeight(Integer weight) {
	this.weight = weight;
}
@Override
public String toString() {
	return "Distributions [assignmentCategory=" + assignmentCategory + ", weight=" + weight + "]";
}


}
