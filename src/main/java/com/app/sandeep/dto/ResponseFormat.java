package com.app.sandeep.dto;

public class ResponseFormat {
	private String subject;
	private String testScore;
	private String 	quizScore;
	private String labScore;
	private String projectScore;
	private String overallRating;
	private String studentName;
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getTestScore() {
		return testScore;
	}
	public void setTestScore(String testScore) {
		this.testScore = testScore;
	}
	public String getQuizScore() {
		return quizScore;
	}
	public void setQuizScore(String quizScore) {
		this.quizScore = quizScore;
	}
	public String getLabScore() {
		return labScore;
	}
	public void setLabScore(String labScore) {
		this.labScore = labScore;
	}
	public String getProjectScore() {
		return projectScore;
	}
	public void setProjectScore(String projectScore) {
		this.projectScore = projectScore;
	}
	public String getOverallRating() {
		return overallRating;
	}
	public void setOverallRating(String overallRating) {
		this.overallRating = overallRating;
	}
	public String getStudentName() {
		return studentName;
	}
	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}
	public ResponseFormat() {
		super();
	}
	public ResponseFormat(String subject, String testScore, String quizScore, String labScore, String projectScore,
			String overallRating, String studentName) {
		super();
		this.subject = subject;
		this.testScore = testScore;
		this.quizScore = quizScore;
		this.labScore = labScore;
		this.projectScore = projectScore;
		this.overallRating = overallRating;
		this.studentName = studentName;
	}
	@Override
	public String toString() {
		return "ResponseFormat [subject=" + subject + ", testScore=" + testScore + ", quizScore=" + quizScore
				+ ", labScore=" + labScore + ", projectScore=" + projectScore + ", overallRating=" + overallRating
				+ ", studentName=" + studentName + "]";
	}
	
	
	

}
