package com.app.sandeep.controller;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**/
import com.app.sandeep.service.DistributionAssignmentService;

@RestController
public class AssignmentDistributionController {
	@Autowired
	private DistributionAssignmentService service;
	
	@RequestMapping("/name/{name}")
	public Set<?> findByAName(@PathVariable String name){
	System.out.println("findbyname");
		return service.findByName(name);
	}
	
	@RequestMapping("/subject/{subject}")
	public Set<?> findBySubject(@PathVariable String subject){
		
		return service.findBySubject(subject);
	}

		
	
}
