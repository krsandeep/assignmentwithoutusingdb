package com.app.sandeep.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;
import org.springframework.stereotype.Service;

import com.app.sandeep.dto.Assignments;
import com.app.sandeep.dto.Distributions;
import com.app.sandeep.dto.ResponseFormat;


@Service
public class DistributionAssignmentService {
	
	 
	public Set<Assignments> l3 = new LinkedHashSet<>();
	public Set<Assignments> l4 = new LinkedHashSet<>();
	Set<ResponseFormat>res = new HashSet<>();
	Set<ResponseFormat>res1 = new HashSet<>();
	Set<ResponseFormat>res2 = new HashSet<>();
	List<String>nameList = new ArrayList<>();
	ResponseFormat format =null;
	ResponseFormat format1 =null;
	List<Assignments> a1 = new ArrayList<>();
	List<Distributions> d1 = new ArrayList<>();
	 int listCount = 0;
		
		  @PostConstruct public void init() {
		 
		
		a1.add(new Assignments(1,"Ananth","Electro fields","Test_1","21-jul-16",100));
		a1.add(new Assignments(2,"Bhagath","Electro fields","Test_1","21-jul-16",78));
		a1.add(new Assignments(3,"Chaya","Electro fields","Test_1","21-jul-16",68));
		a1.add(new Assignments(4,"Esharath","Electro fields","Test_1","21-jul-16",87));
		a1.add(new Assignments(5,"Bhagath","Electro fields","quiz_1","22-jul-16",20));
		a1.add(new Assignments(6,"Chaya","Electro fields","lab_1","23-jul-16",10));
		a1.add(new Assignments(7,"Ananth","Electro fields","project_1","24-jul-16",100));
		a1.add(new Assignments(8,"Davanth","Electro fields","project_1","24-jul-16",100));
		a1.add(new Assignments(9,"Bhagath","Electro fields","quiz_2","25-jul-16",50));
		a1.add(new Assignments(10,"Ananth","Electro fields","quiz_1","26-jul-16",100));
		a1.add(new Assignments(11,"Bhagath","Electro fields","lab_1","27-jul-16",10));
		a1.add(new Assignments(12,"Chaya","Electro fields","project_1","28-jul-16",100));
		a1.add(new Assignments(13,"Bhagath","Electro fields","project_1","28-jul-16",100));
		a1.add(new Assignments(14,"Ananth","Computing Techniques","Test_1","29-jul-16",86));
		a1.add(new Assignments(15,"Ananth","Electro fields","quiz_2","29-jul-16",100));
		a1.add(new Assignments(16,"Bhagath","Computing Techniques","project_1","30-jul-16",100));
		a1.add(new Assignments(17,"Ananth","Electro fields","lab_1","30-jul-16",100));
		a1.add(new Assignments(18,"Chaya","Computing Techniques","quiz_1","31-jul-16",20));
		a1.add(new Assignments(19,"Ananth","Electro fields","Test_2","1-aug-16",100));
		a1.add(new Assignments(20,"Chaya","Electro fields","Test_2","1-aug-16",92));
		

		d1 .add(new Distributions("Test", 40));
		d1 .add(new Distributions("Quiz", 20));
		d1 .add(new Distributions("lab work", 10));
		d1 .add(new Distributions("Project", 30));
		

	}
	
	public Set<?> findByName(String name){
		System.out.println("service "+name);
		for(Assignments tmpData :a1) {
			if(tmpData.getStudentName().trim().equalsIgnoreCase(name)) {
				l3.add(tmpData);
				System.out.println(l3.toString());
			}
		}
		float test=0,lab=0,project=0,projectSec=0,quiz=0,testTechniques=0;
		float testTechTotal=0,quizSec=0,labSec=0,testSec=0,comProject=0,comProSec=0,quizCom=0,quizComSce=0;
		int testCount=0,qquiz=0,quizComCount=0/*,testComputingCount=0*/;
		if(!res.isEmpty() && res!=null)
			res.clear();
		if(!l3.isEmpty() && l3!=null) {
			try {
				for(Assignments list:l3) {
					if(list.getSubject().equalsIgnoreCase("Electro fields")) {
						if(list.getAssignmentCategory().contains("Test")) {
							test =list.getPoints();
							if(testCount>0) {
								testSec=testSec/2*100;
								test = 40*test/2;
								testSec = (testSec+test)/100;
							}if(testCount==0) {
								testSec+=40*test/100;
								testCount++;
							}
							System.out.println(testSec);
						}



						else if(list.getAssignmentCategory().contains("quiz")) {
							quiz=list.getPoints();
							if(qquiz>0) {
								quizSec = quizSec/2*100;
								quiz=20*quiz/2;
								quizSec = (quizSec+quiz)/100;
							}
							if(qquiz==0) {
								quizSec+=20*quiz/100;
								qquiz++;
								System.out.println(quizSec);
							}
						}
						else if(list.getAssignmentCategory().contains("project")) {
							project=list.getPoints();
							projectSec+=30*project/100;
							System.out.println("project "+project+projectSec);
						}
						else if(list.getAssignmentCategory().contains("lab")) {
							lab=list.getPoints();
							labSec+=10*lab/100;
							System.out.println(labSec);

						}

					}else if(list.getSubject().equalsIgnoreCase("Computing Techniques")) {


						if(list.getAssignmentCategory().contains("Test")) {
							testTechTotal = list.getPoints();
							testTechniques+=40*testTechTotal/100;
						}
						else if(list.getAssignmentCategory().contains("project")) {
							comProject=list.getPoints();
							comProSec+=30*comProject/100;
						}else if(list.getAssignmentCategory().contains("quiz")) {
							quizCom=list.getPoints();
							if(quizComCount>0) {
								quizComSce =20*quizCom/100;
								quizCom=20*quizCom/2;
								quizComSce = (quizComSce+quizCom)/100;
							}
							if(quizComCount==0) {
								quizComSce+=20*quizCom/100;
								quizComCount++;

							}


						}

					}




				}
				format = new ResponseFormat();
				
				format.setTestScore(String.valueOf(testSec));
				format.setProjectScore(String.valueOf(projectSec));
				format.setQuizScore(String.valueOf(quizSec));
				format.setLabScore(String.valueOf(labSec));
				format.setOverallRating(String.valueOf(testSec+projectSec+quizSec+labSec));
				format.setSubject("Electro fields");
				format.setStudentName(name);
				res.add(format);
				format=null;
				format1 = new ResponseFormat();
				format1.setTestScore(String.valueOf(testTechniques));
				format1.setProjectScore(String.valueOf(comProSec));
				//if(quizCom>0 && quizComSce>0 )
				format1.setQuizScore(String.valueOf(quizComSce));
				format1.setLabScore("NA");
				format1.setOverallRating(String.valueOf(testTechniques+comProSec+quizComSce));
				format1.setSubject("Computing Techniques");
				format1.setStudentName(name);
				res.add(format1);
				format1=null;
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
		return  res;
		
	}
	public Set<?> findBySubject(String subject){
		for(Assignments tmpData :a1) {
			if(tmpData.getSubject().trim().equalsIgnoreCase(subject)) {
				l4.add(tmpData);
			}
		}
		  System.out.println("findBySubject "+l4.size());
		  ResponseFormat format = null;
		 if(!res1.isEmpty())
			 res1.clear();
		 if(!nameList.isEmpty())
			 nameList.clear();
		  
		  
		  if(!l4.isEmpty() && l4!=null) {
			  
			  try {
				  System.out.println(l4.toString());
				  
				  for(Assignments list1:l4) {
					  Set <Assignments> details= new HashSet<>();
					 format = new ResponseFormat();
				details.add(list1);
				float test=0,lab=0,project=0,projectSec=0,quiz=0,testTechniques=0;
				float testTechTotal=0,quizSec=0,labSec=0,testSec=0,comProject=0,comProSec=0,quizCom=0,quizComSce=0;
				int testCount = 0, qquiz = 0, quizComCount = 0/* ,testComputingCount=0 */;
				if(!l4.isEmpty() && l4!=null) {
					try {
						for(Assignments list:details) {
							if(!nameList.contains(list.getStudentName())) {
								nameList.add(list.getStudentName());

								if(list.getSubject().equalsIgnoreCase("Electro fields")) {

									if(list.getAssignmentCategory().contains("Test")) {
										test =list.getPoints();
										if(testCount>0) {
											testSec=testSec/2*100;
											test = 40*test/2;
											testSec = (testSec+test)/100;
										}if(testCount==0) {
											testSec+=40*test/100;
											testCount++;
										}
										System.out.println(testSec);
									}



									else if(list.getAssignmentCategory().contains("quiz")) {
										quiz=list.getPoints();
										if(qquiz>0) {
											quizSec = quizSec/2*100;
											quiz=20*quiz/2;
											quizSec = (quizSec+quiz)/100;
										}
										if(qquiz==0) {
											quizSec+=20*quiz/100;
											qquiz++;
											System.out.println(quizSec);
										}
									}
									else if(list.getAssignmentCategory().contains("project")) {
										project=list.getPoints();
										projectSec+=30*project/100;
										System.out.println("project "+project+projectSec);
									}
									else if(list.getAssignmentCategory().contains("lab")) {
										lab=list.getPoints();
										labSec+=10*lab/100;
										System.out.println(labSec);

									}
									format = new ResponseFormat();
									format.setTestScore(String.valueOf(testSec));
									format.setProjectScore(String.valueOf(projectSec));
									format.setQuizScore(String.valueOf(quizSec));
									format.setLabScore(String.valueOf(labSec));
									format.setOverallRating(String.valueOf(testSec+projectSec+quizSec+labSec));
									format.setSubject(list1.getSubject());
									format.setStudentName(list1.getStudentName());
									res1.add(format);
									format=null;
								}

								else if(list.getSubject().equalsIgnoreCase("Computing Techniques")) {


									if(list.getAssignmentCategory().contains("Test")) { 
										testTechTotal =
												list.getPoints(); testTechniques+=40*testTechTotal/100; 
									}
									else if(list.getAssignmentCategory().contains("project")) {
										comProject=list.getPoints(); comProSec+=30*comProject/100;
									}
									else if(list.getAssignmentCategory().contains("quiz")) {
										quizCom=list.getPoints();
										if(quizComCount>0) { quizComSce =20*quizCom/100; quizCom=20*quizCom/2;
										quizComSce = (quizComSce+quizCom)/100; 
										} if(quizComCount==0) {
											quizComSce+=20*quizCom/100; quizComCount++;

										}


									}

									format1 = new ResponseFormat();
									format1.setTestScore(String.valueOf(testTechniques));
									format1.setProjectScore(String.valueOf(comProSec)); //if(quizCom>0 &&quizComSce>0 ) 
									format1.setQuizScore(String.valueOf(quizComSce));
									format1.setLabScore("NA");
									format1.setOverallRating(String.valueOf(testTechniques+comProSec+quizComSce))
									; format1.setSubject(list1.getSubject());
									format1.setStudentName(list1.getStudentName()); res1.add(format1);
									format1=null;

								}


							}
						}
						
						
						 
						 
					}catch(Exception e) {
						e.printStackTrace();
					}
				}

//
				  }
			  }catch(Exception e) {

			  }

		  }else {
			  return new HashSet<String>();
		  }
		  
		  
		  
		  
		  return res1;
	}
		
	

}
